package gurobi;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class RunData {
	
	/**
	 * PARAMETERS
	 */
	static boolean LOCAL_BRANCHING = false;
	static boolean printTable = true;	
	// LB
	static int k = 20;
	static int LBTimeLimit = 6000;

	static String[] DATASETS = {"26"};
	static int[] d1 = {8};
	static int[] d2 = {1};
	static int[] PENALTIES = {20,30,40,50,60,70,80};
	
	public static void main(String[] args) {
		ArrayList<ArrayList<int[]>> solution = null;
		try {
			for(int z=0;z<=0;z++) { 
				for(int i=0; i<PENALTIES.length;i++) {
					
					//Kies voor welke window sizes er gerund moet worden
						// voor alle window sizes: TUPWindows.parseIntDataset(s)*2-2
						for(int w = 6; w<=6; w++) {
							File file;
							if(LOCAL_BRANCHING) {
								file = new File("C:\\Users\\Kenneth\\Desktop\\ResultsForPaper\\"+DATASETS[z]+"\\"+DATASETS[z]+"-"+d1[z]+","+d2[z]+"_w"+w+"_pen"+PENALTIES[i]+"_LB.txt");
							} else {
								file = new File("C:\\Users\\Kenneth\\Desktop\\ResultsForPaper\\"+DATASETS[z]+"\\"+DATASETS[z]+"-"+d1[z]+","+d2[z]+"_w"+w+"_pen"+PENALTIES[i]+".txt");
							}
							if(file.exists()) file.delete();
							if(!file.exists()) file.createNewFile();
							FileWriter fw = new FileWriter(file.getAbsoluteFile());
							BufferedWriter bw = new BufferedWriter(fw);
							if(w!=TUPWindows.parseIntDataset(DATASETS[z])*2-2) {
									int n1 = (TUPWindows.parseIntDataset(DATASETS[z])/2)-d1[z];
									int n2 = (int) ((Math.floor(TUPWindows.parseIntDataset(DATASETS[z])/4))-d2[z]);
									System.out.println(n1); System.out.println(n2);
									try {
										solution = TUPWindows.getTableSolDecomp(DATASETS[z],n1,n2, w,true,PENALTIES[i],LOCAL_BRANCHING,k,LBTimeLimit);
										TUPWindows.writeSolution(bw,TUPWindows.parseIntDataset(DATASETS[z]),solution,w,n1,n2,printTable);
										TUPWindows.relaxed = false;
										TUPWindows.optimized = false;
									} catch (GRBException | NullPointerException e) {
										TUPWindows.writeSolution(bw,TUPWindows.parseIntDataset(DATASETS[z]),w,printTable);
										TUPWindows.relaxed = false;
										TUPWindows.optimized = false;
									}
								
							}
							bw.close();
						}
				}
			}	
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println("FINISHED RUNNING.");
	}

}
